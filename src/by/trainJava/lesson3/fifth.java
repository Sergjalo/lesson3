package by.trainJava.lesson3;

public class fifth {
	public static void main(String[] args) {
		int n =6;
		int [][] f = new int[n][n];
		for (int i =0 ; i<n; i++) {
			f[i]= new int [n];
			if (i%2==0){
				for (int j =0 ; j<n; j++) {
					f[i][j]=j+1;
				}
			} else {
				for (int j =0 ; j<n; j++) {
					f[i][j]=n-j;
				}
			}
		}

		for (int i =0 ; i<n; i++) {
			System.out.println();
			for (int j =0 ; j<n; j++) {
				System.out.print(f[i][j]+" ");
			}
		}
		
	}
}
